import java.util.*;
import person.*;

class Hashmap {
    public static void addPerson(HashMap<Integer, Person>hashmap, String name, int age){
        hashmap.put(hashmap.size(), new Person(name, age));
    }
    public static void main(String[] args){
        HashMap<Integer, Person> hashmap = new HashMap<Integer, Person>();
    
        addPerson(hashmap, "sam", 20);
        addPerson(hashmap, "Ajay", 21);
        addPerson(hashmap, "santhosh", 19);

        for(int id : hashmap.keySet()){
            System.out.println("User Id : " + id + "\n");
            Person person = hashmap.get(id);
            System.out.println("Name : " + person.name + "\nAge : " + person.age + "\n");
        }
    }
}
