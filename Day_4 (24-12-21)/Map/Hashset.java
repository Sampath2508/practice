import java.util.HashSet;

class Hashset {
    public static void main(String[] arg){
        HashSet<String> hashset = new HashSet<String>();

        hashset.add("sam");
        hashset.add("ajay");
        hashset.add("santhos");

        System.out.println("HashSet : " + hashset);
        System.out.println(hashset.contains("sam"));
    }
}
