import java.io.*;

class Bufferedoutputstream {
    public static void main(String[] arg){
        try{
            FileOutputStream fos = new FileOutputStream("files//file.txt");
            BufferedOutputStream bos = new BufferedOutputStream(fis);
            String str = "this is example program,\nfor bufferedwriter";
            byte[] b = str.getBytes();
            bos.write(b);
            bos.close();
            fos.close();
            
            fis.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
