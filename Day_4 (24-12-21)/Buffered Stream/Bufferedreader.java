import java.io.*;

class Bufferedreader {
    public static void main(String[] arg){
        try{
            FileReader fr = new FileReader("files//file.txt");
            BufferedReader br = new BufferedReader(fr);

            for(int i = br.read(); i != -1; i = br.read()){
                System.out.print((char) i);
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
