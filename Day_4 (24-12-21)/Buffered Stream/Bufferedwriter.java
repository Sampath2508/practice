import java.io.*;

class Bufferedwriter {
    public static void main(String[] arg){
        try{
            FileWriter fr = new FileWriter("files//file.txt");
            BufferedWriter bw = new BufferedWriter(fr);

            bw.write("this is example program,\nfor bufferedwriter");

            bw.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
