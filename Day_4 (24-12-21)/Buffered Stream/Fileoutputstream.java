import java.io.*;

class Fileoutputstream {
    public static void main(String[] arg){
        try{
            String str = "this is example program,\nfor bufferedwriter";
            FileOutputStream fos = new FileOutputStream("files//file.txt");
            byte[] b = str.getBytes();  
            fos.write(b);
            fos.close();
        }
        catch(IOException e){
            System.out.println(e);
        }

    }
}
