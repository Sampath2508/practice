import java.io.*;

class Filereader{
    public static void main(String[] arg){
        try{
            FileReader fr = new FileReader("files//file.txt");
            int i;
            while((i = fr.read()) != -1){
                System.out.print((char) i);
            }
            fr.close();        
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
