import java.io.*;

class Fileinputstream {
    public static void main(String[] arg){
        try{
            FileInputStream fis = new FileInputStream("files//file.txt");  
            int i = 0;
            while((i = fis.read()) != -1) System.out.print((char)i);
            fis.close();
        }
        catch(IOException e){
            System.out.println(e);
        }

    }
}
