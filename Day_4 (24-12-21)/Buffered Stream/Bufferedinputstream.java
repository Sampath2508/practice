import java.io.*;

class Bufferedinputstream {
    public static void main(String[] arg){
        try{
            FileInputStream fis = new FileInputStream("files//file.txt");
            BufferedInputStream bis = new BufferedInputStream(fis);
            int i;
            while((i = bis.read()) != -1) System.out.print((char) i);
            bis.close();
            fis.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
