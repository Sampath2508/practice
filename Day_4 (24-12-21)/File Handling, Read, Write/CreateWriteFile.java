import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

class CreateWriteFile {
    public static void main(String[] arg){
        try{
            File file = new File("files//file.txt");

            if(file.createNewFile()) System.out.println("File Created Name : " + file.getName());
            else System.out.println("File Already Exist....");

            FileWriter fwrite = new FileWriter("files//file.txt");

            System.out.println("Writing...");
            fwrite.write("Hello everyone....");

            fwrite.close();
            System.out.println("File Closed Successfully...");

        }
        catch(IOException e){
            System.out.println("Error occured : " + e);
        }      


    }
}
