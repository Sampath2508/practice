import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class ReadFile {
    public static void main(String[] arg){

        try{
            File file = new File("files//file.txt");

            Scanner print = new Scanner(file);

            while(print.hasNextLine()) System.out.println(print.nextLine());

            print.close();
        }
        catch(FileNotFoundException e){
            System.out.println("Error Occured : " + e);
        }
        
    }
}
