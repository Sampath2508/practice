class Node{
    int data;
    Node next;
    public  Node(int data){
        this.data = data;
        next = null;
    }
}

class Solution{
    public static void printList(Node head){
        while(head.next != null){
            System.out.print(head.data+ " -> ");
            head = head.next;
        }
        System.out.print(head.data);
    }
    public static void main(String[] arg){
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(5);
        
        printList(head);

    }
}