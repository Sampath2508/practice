// import java.util.List;
import java.util.*;

class Solution {
    public static void main(String[] arg){
        // List<Integer> arr = new ArrayList<Integer>();

        List<Integer> arr = new ArrayList<Integer>();

        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);

        System.out.println("Array : " + arr);
        System.out.println("Array Length : " + arr.size());
        System.out.println("Get the Value in index 0 : " + arr.get(0));
        arr.set(0, 10);
        System.out.println("Set the Value in index 0 : " + arr.get(0));
    }
}
