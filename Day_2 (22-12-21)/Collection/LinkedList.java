import java.util.LinkedList;

class Solution {
    public static void main(String[] arg){
        LinkedList<Integer> list = new LinkedList<Integer>();
        
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        System.out.println("list : " + list);

        System.out.println("list Length : " + list.size());

        // To add the element to beginning
        list.addFirst(1);

        // To add the element to end
        list.addLast(2);

    
        System.out.println("To Get the first element : " + list.getFirst());

        System.out.println("To Get the last element : " + list.getLast());

        // To remove first element
        list.removeFirst();

        // TO remove last Element
        list.removeLast();


        System.out.println("list : " + list);

    }    
}
