import java.util.Stack;

class Solution{
    public static void main(String[] arg){
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        System.out.println("Stack : " + stack);

        System.out.println("Stack length : " + stack.size());

        System.out.println("Return Top element of the Stack : " + stack.peek());

        System.out.println("Return and remove Top element of the Stack : " + stack.pop());

        System.out.println("To check stack is empty or not : " + stack.empty());      
        
    }  
}

