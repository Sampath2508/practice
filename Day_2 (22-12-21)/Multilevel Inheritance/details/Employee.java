package details;

import details.*;

public class Employee extends Person{
    public Employee(String name, int id){
        super(name, id);
    }

    public void displayDetail(){
        System.out.println("Employee Detail :\nEmpId : " + super.id + "\nEmpName : " + super.name);
    }
}
