package details;

import details.*;

public class Customer extends Person{
    public Customer(String name, int id){
        super(name, id);
    }

    public void displayDetail(){
        System.out.println("Customer Detail :\nCusId : " + super.id + "\nCusName : " + super.name);
    }
}
