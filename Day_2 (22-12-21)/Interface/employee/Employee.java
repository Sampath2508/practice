package employee;

import java.util.Scanner;

interface CollectingDetail{
    public void getDetail();
    public void displayDetail();
}

public class Employee implements CollectingDetail{
    public String empName;
    public int empId;
    public Scanner input;
    public Employee(){
        this.input = new Scanner(System.in);
    }

    public void getDetail(){
        System.out.print("Enter Your Name : ");
        this.empName = this.input.nextLine();

        System.out.print("\nEnter Your Id : ");
        this.empId = this.input.nextInt();
    }

    public void displayDetail(){
        System.out.println("Id : " + this.empId + "\nName : " + this.empName);
    }
    
}
