import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

class WriteXml{
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        Document document = db.newDocument();

        Element root = document.createElement("students");
        document.appendChild(root);

        Element stud = document.createElement("student");
        root.appendChild(stud);

        // Attr attr = document.createAttribute("id");
        // attr.setValue("1");

        stud.setAttribute("id", "1");

        Element name = document.createElement("name");
        name.appendChild(document.createTextNode("sam"));

        Element password = document.createElement("password");
        password.appendChild(document.createTextNode("sam123"));

        Element email = document.createElement("email");
        email.appendChild(document.createTextNode("sam@fake.com"));

        Element department = document.createElement("department");
        department.appendChild(document.createTextNode("IT"));

        stud.appendChild(name);
        stud.appendChild(password);
        stud.appendChild(email);
        stud.appendChild(department);

        TransformerFactory tff = TransformerFactory.newInstance();
        Transformer tf = tff.newTransformer();

        DOMSource ds = new DOMSource(document);

        StreamResult sr = new StreamResult(new FileWriter("Files//stu.xml"));
        tf.transform(ds, sr);      
    }
}
