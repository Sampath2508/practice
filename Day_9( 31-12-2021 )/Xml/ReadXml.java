import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

class ReadXml {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, FileNotFoundException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        Document document = db.parse(new File("Files//students.xml"));

        document.getDocumentElement().normalize();

        NodeList Students = document.getElementsByTagName("student");
        
        for(int i = 0; i < Students.getLength(); i++){
            Node student = Students.item(i);
            if(student.getNodeType() == Node.ELEMENT_NODE){
                Element detail = (Element) student;
                System.out.println("\nStudent Id : " + detail.getAttribute("id"));
                System.out.println("firstname : " + detail.getElementsByTagName("name").item(0).getTextContent());
                System.out.println("lastname : " + detail.getElementsByTagName("password").item(0).getTextContent());
                System.out.println("nickname : " + detail.getElementsByTagName("email").item(0).getTextContent());
                System.out.println("marks : " + detail.getElementsByTagName("department").item(0).getTextContent());
            }
        }
    }
}
