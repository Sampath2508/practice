
class ThreadOne implements Runnable{
    @Override
    public void run(){
        for(int i = 0; i <= 5; i++){
            System.out.println("ThreadOne ---- " + i);
        }
    }
}

class ThreadTwo implements Runnable{
    @Override
    public void run(){
        for(int i = 0; i <= 5; i++){
            System.out.println("ThreadTwo ---- " + i);
        }
    }
}


class Multithreading{
    public static void main(String[] arg){
        Thread thread1 = new Thread(new ThreadOne());
        Thread thread2 = new Thread(new ThreadTwo());

        thread1.start();
        thread2.start();
    }
}
