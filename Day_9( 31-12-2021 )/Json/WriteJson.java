import org.json.simple.*;
import java.io.*;

class WriteJson {
    public static void main(String[] arg){
        JSONObject stu1 = new JSONObject();
        stu1.put("id", 1);
        stu1.put("name", "Sam");
        stu1.put("age", 21);

        JSONObject stu2 = new JSONObject();
        stu2.put("id", 2);
        stu2.put("name", "deepak");
        stu2.put("age", 20);

        JSONArray arr = new JSONArray();
        arr.add(stu1);
        arr.add(stu2);

        JSONObject Students = new JSONObject();
        Students.put("students", arr);

        try{
            System.out.println("Writing...");
            FileWriter file = new FileWriter("Files//student.json");
            file.write(Students.toJSONString());
            file.close();
        }catch(IOException e){
            e.printStackTrace();
        }
       
    }
}
