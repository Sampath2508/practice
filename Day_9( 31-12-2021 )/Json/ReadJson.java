import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

class ReadJson{
    public static void main(String[] arg) throws FileNotFoundException, IOException, ParseException{
        JSONParser parser = new JSONParser();
        Object obj = parser.parse( new FileReader("Files//student.json"));
        JSONObject stud = (JSONObject) obj;        
        JSONArray studArr = (JSONArray) stud.get("students");

        for(int ind = 0; ind < studArr.size(); ind++){
            JSONObject cur = (JSONObject) studArr.get(ind);
            Long id = (Long) cur.get("id");
            String name = (String) cur.get("name");
            Long age = (Long) cur.get("age");
            System.out.println(String.format("ID : %o\nName : %s\nAge : %o\n", id, name, age));
        }
    }
}