import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Constructor;

class Testing{
    public String name;
    public Testing(){
        name = "Sam";
    }

    public void method1(){
        System.out.println("Method 1......");
    }

    public void method2(){
        System.out.println("Method 2......");
    }

    public void method3(String dis){
        System.out.println(dis);
    }
}

class Reflection {
    public static void main(String[] arg) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException{
        Testing test = new Testing();
        
        Class cls = test.getClass();
        System.out.println(cls.getName());

        Constructor con = cls.getConstructor();
        System.out.println(con.getName());

        Method[] method = cls.getMethods();

        for(Method m : method){
            System.out.println(m.getName());
        }

        Method testMethod = cls.getDeclaredMethod("method3", String.class);

        testMethod.invoke(test, "Print this Line....");

        Field name = cls.getDeclaredField("name");
        name.setAccessible(true);
        name.set(test, "sampath");

        System.out.println(test.name);
    }
}
