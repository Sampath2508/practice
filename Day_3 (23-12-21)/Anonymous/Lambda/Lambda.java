import  math.*;

class Solution {

    public static void main(String[] args){
        Addition addition = (x, y) -> { return x + y; };
        Subtraction subtraction = (x, y) -> { return x - y; };

        System.out.println("Add of two number : " + addition.add(10, 5));
        System.out.println("Minus of two number : " + subtraction.minus(10, 5));
    }
}
