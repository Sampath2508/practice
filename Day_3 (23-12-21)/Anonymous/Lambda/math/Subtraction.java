package math;

@FunctionInterface
public interface Subtraction{
    int minus(int num1, int num2);
}
