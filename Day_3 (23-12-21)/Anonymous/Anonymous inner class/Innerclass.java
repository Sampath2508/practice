import person.*;

class Innerclass {
    public static void main(String[] arg){
        Person person = new Person(){
            @Override
            void printName(String name){
                System.out.println("Name : " + name);
            }
        };
        person.printName("Sam");
        person.printName("Sampath");
    }
}
