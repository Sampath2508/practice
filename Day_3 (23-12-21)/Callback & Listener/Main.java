import callback.*;

class Main{
    public static void main(String[] arg){
        Callback cb = new Callback();
        Listener listener = new CallbackFunction();
        cb.registerLister(listener);
        cb.doTask();
    }
}