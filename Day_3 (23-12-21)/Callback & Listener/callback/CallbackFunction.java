package callback;
import callback.*;

public class CallbackFunction implements Listener{
    @Override
    public void onCallback(){
        System.out.println("Callback is executed....");
    }
}
