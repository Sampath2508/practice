package callback;
import callback.*;

import java.io.*;
import java.lang.Thread;

public class Callback {
    public Listener listener;

    public void registerLister(Listener listener){
        this.listener = listener;
    }

    public void doTask(){

        System.out.println("Async task started....");
        System.out.println("callback run after 5s");
        try{
            Thread.sleep(5000);
        }catch(Exception e){
            System.out.println(e);
        }
        if(this.listener != null){
            listener.onCallback();
        }
    }   
}
