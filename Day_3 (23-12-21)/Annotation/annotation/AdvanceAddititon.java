package annotation;
import annotation.*;

public class AdvanceAddition extends Addition{

    @Deprecated
    public static void show(){
        System.out.println("Don't use this version");
    }

    @Override
    public int add(int x, int y, int z){
        return x + y + z;
    }
}